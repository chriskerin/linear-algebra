import numpy as np

# define matricies
A = np.matrix([[1,1],[2,4]])
B = np.matrix([[35],[94]])

# find the inverse of A
A_inverse = np.linalg.inv(A)
# print(A_inverse)

# Find the solution to Ax = B
X = A_inverse * B
print(X)
